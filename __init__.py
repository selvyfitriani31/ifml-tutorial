#!/usr/bin/python3

from main.core.angular.main import generate_project
import logging
from ifml_parser.ifmlxmiparser import parse
from custom_xmi_parser.xmiparser_2 import parse as uml_parse
import json
import os
import sys
from main.core.react.main import generate_project_multi
logging.basicConfig(level=logging.DEBUG)

# client_id =\
#    '885419871838-o5dskm8ri6bt2c85s1abmo1vob73o7hl.apps.googleusercontent.com'
client_id =\
    '980984936575-0lo321pevqjlul7nsdk441ccjah11b1f.apps.googleusercontent.com'

mapping = json.load(open('mapping.json'))


def create_mapping(input_file):
    result = {}
    result["product_name"] = input_file["product_name"]
    result["origin_name"] = input_file["original_product_name"]
    result["product_address"] = input_file["full_product_address"]
    result["enable_login"] = (input_file["auth"]["type"].lower() in [
                              "google", "facebook", "auth0"])
    result["google_oauth2_client_id"] = input_file["auth"].get(
        "client_id", client_id)
    result["domain_name"] = input_file["domain_name"]
    result["client_id"] = input_file["auth"]["client_id"]
    result["client_secret"] = input_file["auth"]["client_secret"]
    result["client_type"] = input_file["auth"]["type"]
    result["role"] = input_file["roles"]
    result["instance_port"] = input_file["ports"]["frontend"]
    result["public_port"] = input_file["ports"]["nginx"]
    result["generator_home"] = input_file["generator_home_address"]
    result["google_analytics_id"] = input_file["google_analytics_id"]
    result["selected_feature"] = {}
    result["json_path"] = None
    for feature in input_file["features"]:
        map_name = feature.split("{")[0]
        if map_name in mapping:
            result["selected_feature"][map_name] = []
            result["selected_feature"][map_name].append(mapping[map_name][0])
            result["selected_feature"][map_name].append(mapping[map_name][1])
    return result


input_file = sys.argv[1]
project_type = sys.argv[2]
with open(input_file) as f:
    data = json.load(f)
    input_file = create_mapping(data)
    if not os.path.exists("./result"):
        os.makedirs("./result", exist_ok=True)
    if project_type == "react":
        generate_project_multi(
            **input_file, remove_folder_content=False, target_directory="./result")
    else:
        generate_project(**input_file, target_directory="./result")
