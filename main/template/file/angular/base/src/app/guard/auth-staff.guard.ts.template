import {
    Injectable
} from '@angular/core';
import {
    Router,
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';

import {
    environment
} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class StaffAuthGuard implements CanActivate {

    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let staffRole = environment.role.staff;
        
        if (staffRole.includes(localStorage.getItem('email'))) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/error-page-no-permission'], {
            queryParams: {
                returnUrl: state.url
            }
        });
        return false;
    }
}