from main.utils.ast.base import Node
from main.utils.jinja.angular import app_components_writer
from main.utils.ast.framework.angular.components import AngularComponent
from main.utils.ast.framework.angular.routers import RouteToModule


class HalamanBerhasilKonfirmasiDonasiTypescriptClass(Node):

    def __init__(self, url, domain_name, client_id, client_secret):
        super().__init__()
        self.selector_name = 'halaman-berhasil-konfirmasi-donasi'
        self.class_name = 'HalamanBerhasilKonfirmasiDonasi'
        self.url = url
        self.domain_name = domain_name
        self.client_id = client_id
        self.client_secret = client_secret

    def render(self):
        return app_components_writer('halaman-berhasil-konfirmasi-donasi/halaman-berhasil-konfirmasi-donasi.component.ts.template', url=self.url, domain_name=self.domain_name, client_id=self.client_id, client_secret=self.client_secret)

    def get_class_name(self):
        return self.class_name


class HalamanBerhasilKonfirmasiDonasiHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return app_components_writer('halaman-berhasil-konfirmasi-donasi/halaman-berhasil-konfirmasi-donasi.component.html.template')


def __set_up_halaman_berhasil_konfirmasi_donasi_template_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node, url, domain_name, client_id, client_secret):
    html, typescript_class = HalamanBerhasilKonfirmasiDonasiHTML(), HalamanBerhasilKonfirmasiDonasiTypescriptClass(
        url, domain_name=domain_name, client_id=client_id, client_secret=client_secret)
    angular_component_node = AngularComponent(typescript_class, html)
    # routing_node = RouteToModule(typescript_class)
    # root_rooting.add_children_routing(routing_node)

    basic_app_module.add_component_to_module(angular_component_node)

    # Importing all component to the routing node
    basic_routing.register_component_with_router(angular_component_node)

    # Insert the component definition into src folder
    basic_template.add_new_component_using_basic_component_folder(
        angular_component_node.build())
